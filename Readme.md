WELLS: Multi-well variable-rate pumping-test analysis tool
======

![](logo/wells-logo.png)

* WELLS is a C code for multi-well variable-rate pumping test analysis based on analytical methods.
* WELLS computes drawdown in confined, unconfined and leaky aquifers through a variety of analytical solutions.
* WELLS considers fully or partially penetrating pumping and observation wells. It also includes wellbore storage capacity of pumping wells.
* WELLS can simulate variable-rate pumping tests where the variable rate is approximated either as piecewise linear or as step changes. It can also handle standard exponential and sinusoidal changes in pumping rates.
* WELLS utilizes the principle of superposition to account for transients in the pumping regime and to include multiple sources/sinks (e.g. pumping wells).
* WELLS combines the use of the principle of superposition and method of images to represent constant head or no flow boundaries.
* WELLS accounts for an exponential or linear temporal trend in simulated water levels to account for non-pumping influences (e.g. long-term aquifer recharge or discharge trend).
* WELLS is a unix-style code with command-line interface. It has been tested and applied using various operating systems (Microsoft Windows (32/64), Cygwin, Linux and Mac OS X).

Theis vs Mishra
------

Comparison of the Theis and Mishra solutions.

<img src="compare/theis_vs_mishra.png" alt="drawing" width="600"/>

Installation
---
```
brew install gsl
make
```

Input file description:

```
Problem name: test
Aquifer type        : 1
Boundary information: 1 1 0 0 0
--- Number of  wells: 7 # NUMBER OF PUMPING WELLS
PM-1	502229.3994	538920.574176	0.3048	1455 #  wellname, X, Y, radius, number of pumping-rate time steps
Initial Head        : 1780
Aquifer thickness   : 1
Permeability        : 5.54439259209296
Storage coefficient : -1.0000000000000
Leakage coefficient : -10
38535	3141.89240343413 # time 1, pumping rate
38536	3194.28251290826 # time 2, pumping rate
...

--- Number of points: 3 # NUMBER OF OBSERVATION WELLS
R-11 499859.597688    539298.968136        984 #  wellname, X, Y, number of compute time steps
Initial Head        : 1779.688325597366
Permeability        : 4.86310103590660
Storage coefficient : -1.0000000000000
38535.0006944444 # time 1
38536.0006944444 # time 2

....

R-15 498442.06128    538969.458        1000 # wellname, X, Y, number of compute time steps
Initial Head        : 1783.536642041219
Permeability        : 4.90700088894812
Storage coefficient : -2.4689653751222
38535.0006944444 # time 1
38536.0006944444 # time 2
38537.0006944444 # time 3
38538.0006944444 # time 4

...

```


Alternatives
-----

* wells -
* wells_exp -
* wells_hw  - production well heterogeneity
* wells_hwp - production and observation well heterogeneity

 LA-UR-11-11478; LA-CC-10-019; LA-CC-11-098
